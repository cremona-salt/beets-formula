# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import beets with context %}
{%- set sls_config_file = tplroot ~ '.config.file' %}

{%- set mountpoint = beets.docker.mountpoint %}

include:
  - {{ sls_config_file }}

beets-container-running-container-present:
  docker_image.present:
    - name: {{ beets.container.image }}
    - tag: {{ beets.container.tag }}
    - force: True

beets-container-running-container-managed:
  docker_container.running:
    - name: {{ beets.container.name }}
    - image: {{ beets.container.image }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/beets/config:/config
      - {{ mountpoint }}/shares/audio/music:/music
      - {{ mountpoint }}/shares/audio/incoming_music:/downloads
    - port_bindings:
      - 8337:8337
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - watch:
      - sls: {{ sls_config_file }}
