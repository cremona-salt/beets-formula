# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import beets with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = beets.docker.mountpoint %}
{%- set apikey = beets.acoustid.apikey %}

{%- for dir in [
  'docker/beets/config',
  'shares/audio/music',
  'shares/audio/incoming_music'
  ]
%}

beets-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

beets-config-file-beets-config-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/beets/config/config.yaml
    - source: {{ files_switch(['config.yaml.tmpl'],
                              lookup='beets-config-file-beets-config-managed',
                 )
              }}
    - user: stooj
    - group: users
    - mode: 644
    - makedirs: True
    - template: jinja
    - context:
        beets: {{ beets }}
        apikey: {{ apikey }}
